package main
import (
 "fmt"
 "net/http"
 "os"
)
func main() {
 var PORT string
 if PORT = os.Getenv("PORT"); PORT == "" {
  PORT = "3001"
 }
 http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
 fmt.Fprintf(w, "<h1>Hallo Welt!!!</h1> Build in Wermelskirchen as a Go-App </br> Maintained by Florian Stoeber </br> Find the source code of the app, the Dockerfile and the CI/CD pipeline in this repository: https://bitbucket.org/florianstoeber/goapp")
 })
 http.ListenAndServe(":" + PORT, nil)
}